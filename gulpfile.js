const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const less = require('gulp-less');
const rimraf = require('rimraf');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const post_autoprefixer = require('autoprefixer');
// const cssnano = require('cssnano');
const gulpif = require('gulp-if');
const webpackStream = require('webpack-stream');
const webpack = require('webpack');
const gcmq = require('gulp-group-css-media-queries');
const cleanCSS = require('gulp-clean-css');

let isDev = (process.argv.indexOf('--dev') !== -1);
let isProd = !isDev;

let isPug = false;

// ********************* SIMPLE TASKS *******************

//CLEAR build
gulp.task('clean', function del(cb) {
    return rimraf('build', cb);
});

// Static server
gulp.task('server', function() {
    browserSync.init({
        server: {
            port: 9000,
            baseDir: "build" //куда будет смотреть сервер
        }
    });

    gulp.watch('build/**/*').on('change', browserSync.reload);
});

// JS
let webConfig = {
    output: {
        filename: 'main.min.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: '/node_modules/'
            }
        ]

    },
    // plugins: [
    //     new webpack.ProvidePlugin({
    //         $: 'jquery',
    //         jQuery: 'jquery',
    //         'window.jQuery': 'jquery'
    //     })
    // ],
    mode: isDev ? 'development' : 'production',
    devtool: isDev ? 'eval-source-map' : 'none'
};
gulp.task('js', function () {
    return gulp.src('src/js/main.js')
        .pipe(webpackStream(webConfig))
        .pipe(gulp.dest('build/js'));
});

// PUG or HTML
gulp.task('html', function buildHTML() {
    return gulp.src(isPug ? 'src/*.pug' : 'src/*.html')
        .pipe(gulpif(isPug, pug({
            pretty: true
        })))
        .pipe(gulp.dest('build'))
});

// SASS

gulp.task('sass', function () {
    let plugins = [
         post_autoprefixer()
         // cssnano()
    ];
    return gulp.src('src/styles/main.sass')
        .pipe(gulpif(isDev, sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(gcmq())
        .pipe(postcss(plugins))
        .pipe(gulpif(isProd, cleanCSS({
            level: 2
        })))
        .pipe(rename('main.min.css'))
        .pipe(gulpif(isDev, sourcemaps.write()))
        .pipe(gulp.dest('build/css'));
});

// LESS
gulp.task('less', function () {
    let plugins = [
        post_autoprefixer()
        // cssnano()
    ];
    return gulp.src('src/styles/main.less')
        .pipe(gulpif(isDev, sourcemaps.init()))
        .pipe(less())
        .pipe(gcmq())
        .pipe(postcss(plugins))
        .pipe(gulpif(isProd, cleanCSS({
            level: 2
        })))
        .pipe(rename('main.min.css'))
        .pipe(gulpif(isDev, sourcemaps.write()))
        .pipe(gulp.dest('build/css'));
});

//COPY fonts
gulp.task('copyFonts', function () {
    return gulp.src('src/fonts/**/*.*')
        .pipe(gulp.dest('build/fonts'))
});

//COPY images
gulp.task('copyImages', function () {
    return gulp.src('src/img/**/*.*')
        .pipe(gulp.dest('build/img'))
});

// COPY audio
gulp.task('copyAudio', function () {
    return gulp.src('src/audio/**/*.*')
        .pipe(gulp.dest('build/audio'))
});

// COPY video
gulp.task('copyVideo', function () {
  return gulp.src('src/video/**/*.*')
    .pipe(gulp.dest('build/video'))
});

// *********************** CASCADE TASKS **********

// Full COPY
gulp.task('copy', gulp.parallel('copyFonts', 'copyImages', 'copyAudio', 'copyVideo'));

// Watchers
gulp.task('watch', function () {
    gulp.watch('src/**/*.pug', gulp.series('html'));
    gulp.watch('src/**/*.html', gulp.series('html'));
    gulp.watch('src/styles/**/*.sass', gulp.series('sass'));
    gulp.watch('src/styles/**/*.less', gulp.series('less'));
    gulp.watch('src/js/**/*.js', gulp.series('js'));
    gulp.watch('src/templates/blocks/**/*.js', gulp.series('js'));
    gulp.watch('src/templates/blocks/**/*.sass', gulp.series('sass'));
    gulp.watch('src/templates/blocks/**/*.less', gulp.series('less'));
});

// ****************** WORK TASKS ***************************

//BUILD
gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('html', 'sass', 'js', 'copy')
    )
);

gulp.task('build_less', gulp.series(
    'clean',
    gulp.parallel('html', 'less', 'js', 'copy')
    )
);

//DEVELOP
gulp.task('default', gulp.series(
    'build',
    gulp.parallel('watch', 'server')
    )
);

gulp.task('dev_less', gulp.series(
    'build_less',
    gulp.parallel('watch', 'server')
    )
);