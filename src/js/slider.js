export default function (el, autoplay = true) {
  let $dots = el.querySelectorAll('.slider__dot');
  let $container = el.querySelector('.slider__container');
  let $slides = el.querySelector('.slider__slides');
  let width = $container.offsetWidth;
  let current = 0;
  let interval;
  for (let i = 0; i < $dots.length; i++) {
    $dots[i].addEventListener('click', function (e) {
      if(e.target.classList.contains('active')) return false;
      changeDot(e.target, $dots);
      current = i;
      changeSlide(i, $slides, width);
      clearInterval(interval)
    });
  }
  if (autoplay) {
    interval = setInterval(function () {
      changeDot($dots[current], $dots);
      changeSlide(current, $slides, width);
      current = current + 1 < $dots.length ? current + 1 : 0;
    }, 3000)
  }
  window.addEventListener('resize', function () {
    setTimeout(function () {
      changeDot($dots[0], $dots);
      width = $container.offsetWidth;
      changeSlide(0, $slides, width)
    }, 500);
  });
}

function changeDot(target, $dots) {
  for (let i = 0; i < $dots.length; i++) {
    $dots[i].classList.remove('active');
  }
  target.classList.add('active');
}

function changeSlide(active, $slides, width) {
  $slides.style.left = -1 * width * active + 'px';
}