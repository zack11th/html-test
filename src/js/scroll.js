export default function (to) {
  let from = self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
  let time = 1000;
  let v = (to - from) / (time / 10);
  let interval = setInterval(function () {
     from += v;
    if ((from <= to && v <= 0)
      || (from >= to && v >= 0)) {
      from = to;
      window.scrollTo(0, from);
      clearInterval(interval);
    }
    window.scrollTo(0, from);
  }, 10)

}