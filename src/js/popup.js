export default function (el) {
  el.addEventListener('click', function () {
    let $popup = document.getElementById(el.dataset.open);
    $popup.classList.add('open');
    document.querySelector('body').style.overflow = 'hidden';
    $popup.addEventListener('click', function (e) {
      if(!e.target.closest('.popup__content')) {
        close($popup);
      }
    });
    let $close = $popup.querySelector('.popup__close');
    $close.addEventListener('click', function () {
      close($popup);
    })
  });
}

function close($popup) {
  $popup.classList.remove('open');
  document.querySelector('body').style.overflow = 'auto';
  let $video = $popup.querySelector('video');
  if ($video) $video.pause();
}