import slider from './slider'
import popup from './popup'
import scroll from './scroll'

window.addEventListener('load', function () {
  // *** slider ***
  let $slider = document.querySelector('#slider');
  slider($slider);

  // *** modal ***
  let $playVideo = document.querySelector('#open-video');
  popup($playVideo);

  // *** btn-up ***
  let $btnUp = document.querySelector('#button-up');
    window.addEventListener('scroll', function () {
    let scrollTop = self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
      if ((scrollTop >= document.documentElement.clientHeight)
      && !$btnUp.classList.contains('show')) {
      $btnUp.classList.add('show');
    } else if ((scrollTop < document.documentElement.clientHeight)
      && $btnUp.classList.contains('show')) {
      $btnUp.classList.remove('show');
    }
  });
  $btnUp.addEventListener('click', function () {
    scroll(0);
  });

  // *** scroll to product ***
  let $product = document.querySelector('.product');
  let $moreInfo = document.querySelector('#more-info');
  $moreInfo.addEventListener('click', function (e) {
    e.preventDefault();
    let product = $product.getBoundingClientRect().top;
    scroll(product);
  });

  // *** mob menu ***
  let $burger = document.querySelector('#burger');
  let $nav = document.querySelector('#head-nav');
  $burger.addEventListener('click', function () {
    $burger.classList.toggle('header__burger--active');
    $nav.classList.toggle('nav--visible');
    if ($nav.classList.contains('nav--visible')) {
      document.querySelector('body').style.overflow = 'hidden';
    } else {
      document.querySelector('body').style.overflow = 'auto';
    }
  });
});